package com.example.demo;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelController {
	
	static GenerateList genList = new GenerateList();
	
	//generic constructor
	public HotelController() {
		
	}
	
	//GET request
	@RequestMapping(value = "/hotels", method = RequestMethod.GET)
	public List<Hotel> get() {
		System.out.println("Hotels " + genList.getList());
		return genList.getList();
		}
	
	//PUT request, @RequestBody binds fields to JSON commands sequence (from Postman)
	@RequestMapping(value= "/hotels/{id}", method = RequestMethod.PUT)
	public String updateHotel(@PathVariable("id") Integer id, @RequestBody Hotel hotel) {
		
		List<Hotel> hotels = GenerateList.getList();
		boolean status  = false;
		for (Hotel h : hotels)
		{
			//try catch
			if (id.equals(h.getId())) {
				
				if (!hotel.getStars().equals(null)) {
					
					h.setStars(hotel.getStars());
					status = true;
				}
			}
			
		}
		
		if (true == status) {			
			return "A functionat!";
		}
		else 			
			return "Mai baga o fisa!";
			
}
			

	

}