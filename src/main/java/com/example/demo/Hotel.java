package com.example.demo;

public class Hotel {
	private String name;
	private String rating;
	private Integer stars;
	private Integer id;
	
	//@return the name
 	public String getName() {
		return name;
	}

	//@param name the name to set
	 public void setName(String name) {
		this.name = name;
	}

	//@return the rating
	public String getRating() {
		return rating;
	}

	//@param rating the rating to set
	public void setRating(String rating) {
		this.rating = rating;
	}

	//@return the stars
	public Integer getStars() {
		return stars;
	}

	//@param stars the stars to set
	public void setStars(int stars) {
		this.stars = stars;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	//empty constructor is required?
	public Hotel() {
		
	}

	public Hotel(int id, String name, String rating, Integer stars) {
		this.id = id;
		this.name = name;
		this.rating = rating;
		this.stars = stars;
	}
	
	@Override
	public String toString() {
		String infoHotel = String.format("Hotel Info: name = %s, rating = %s, stars =%d, id = %d",
                name, rating, stars, id);
		return infoHotel;
	}
	
}
